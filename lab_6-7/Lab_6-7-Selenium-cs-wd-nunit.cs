using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTests
{
    [TestFixture]
    public class Lab67
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        
        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "https://www.blazedemo.com/";
            verificationErrors = new StringBuilder();
        }
        
        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }
        
        [Test]
        public void Lab67Test()
        {
            // Label: Test
            // ERROR: Caught exception [ERROR: Unsupported command [resizeWindow | 1536,665 | ]]
            driver.Navigate().GoToUrl("https://dev.admin.flexy.com/jobs/search");
            driver.FindElement(By.CssSelector("td.ant-table-column-has-actions.ant-table-column-has-sorters.ant-table-row-cell-ellipsis.ant-table-row-cell-break-word")).Click();
            driver.FindElement(By.LinkText("Test CompReqs")).Click();
            driver.FindElement(By.CssSelector("span.non-editable-text")).Click();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Click();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Click();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Clear();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).SendKeys("Test CompReqs2");
            driver.FindElement(By.CssSelector("button.ant-btn.ant-btn-primary.ant-btn-sm")).Click();
            driver.FindElement(By.CssSelector("div.inner-render")).Click();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Clear();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).SendKeys("Test CompReqs");
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).SendKeys(Keys.Enter);
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Clear();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).SendKeys("Test CompReqs");
            driver.FindElement(By.XPath("//div[@id='app']/div[2]/div/div[2]/div/div/div/div/div/div/div/div[7]/div/div/div[2]/div/div/div/div")).Click();
            driver.FindElement(By.CssSelector("li.ant-select-dropdown-menu-item.ant-select-dropdown-menu-item-active")).Click();
            driver.FindElement(By.CssSelector("li.ant-select-dropdown-menu-item.ant-select-dropdown-menu-item-active")).Click();
            driver.FindElement(By.XPath("//div[@id='app']/div[2]/div/div[2]/div/div/div/div/div/div/div/div[7]/div")).Click();
            driver.FindElement(By.CssSelector("div.ant-col.ant-col-13 > div.ant-row > div.ant-col > div.ant-row-flex.ant-row-flex-middle > div.ant-col.ant-col-14.editable-field > div.ant-card-meta > div.ant-card-meta-detail > div.ant-card-meta-title > div.click-to-edit.name > div.inner-render > span.non-editable-text")).Click();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Clear();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).SendKeys("15.25");
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).SendKeys(Keys.Enter);
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Clear();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).SendKeys("15.25");
            driver.FindElement(By.CssSelector("div.ant-col.ant-col-16.editable-field > div.ant-card-meta > div.ant-card-meta-detail > div.ant-card-meta-title > div.click-to-edit.name > div.inner-render > span.non-editable-text")).Click();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Click();
            driver.FindElement(By.CssSelector("button.ant-btn.ant-btn-primary.ant-btn-sm")).Click();
            driver.FindElement(By.CssSelector("div.ant-col.ant-col-16.editable-field > div.ant-card-meta > div.ant-card-meta-detail > div.ant-card-meta-title > div.click-to-edit.name > div.inner-render > span.non-editable-text")).Click();
            driver.FindElement(By.CssSelector("div.ant-col.ant-col-11 > div.ant-row > div.ant-col > div.ant-row-flex.ant-row-flex-middle")).Click();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).Clear();
            driver.FindElement(By.CssSelector("input.ant-input.click-to-edit-input.name")).SendKeys("Midoriya-kun");
            driver.FindElement(By.CssSelector("button.ant-btn.ant-btn-primary.ant-btn-sm")).Click();
            driver.FindElement(By.CssSelector("button.ant-btn.job-details__broadcast-btn")).Click();
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        
        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }
        
        private string CloseAlertAndGetItsText() {
            try {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert) {
                    alert.Accept();
                } else {
                    alert.Dismiss();
                }
                return alertText;
            } finally {
                acceptNextAlert = true;
            }
        }
    }
}
