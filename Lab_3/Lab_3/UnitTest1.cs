using Nunit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace Lab_3
{
    public class Base
    {
        public IWebDriver driver = new ChromeDriver("driver");

        [SetUp]
        public void Setup()
        {
            driver.Url = "http://the-internet.herokuapp.com";
        }

        [TearDown]
        public void Teardown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                string path = @"C:\Users\User\source\repos\Lab_3\results\";
                var imagename = $"results_{DateTime.Now:yyyy-MM-dd_HH-mm-ss.fffff}.png";
                var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(path + imagename);
            }

            driver.Quit();
        }

    }

    public class TestCases : Base
    {

        [Test]
        public void ForgotPassword()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/forgot_password");
            driver.FindElement(By.Id("email")).SendKeys("sdfhgjhgfdsdfdgfshj");
            driver.FindElement(By.Id("form_submit")).Click();
            Assert.AreEqual(driver.FindElement(By.Id("content")).Text, "Your e-mail's been sent!");
        }

        [Test]
        public void Redirection()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/rediraction");
            driver.FindElement(By.Id("redirect")).Click();
            Assert.AreEqual(driver.Url, "http://the-internet.herokuapp.com/status_codes");
        }

        [Test]
        public void OnHover()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/hovers");
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(By.CssSelector("figure")));
            Assert.AreEqual(driver.FindElement(By.CssSelector("figcaption")).Displayed, true);
        }

        [Test]
        public void DynamicContent()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/dynamic_content");
            var text = driver.FindElement(By.XPath("//div[@class='large-10 columns']")).Text;
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/dynamic_content");
            Assert.AreNotEqual(driver.FindElement(By.XPath("//div[@class='large-10 columns']")).Text, text);
        }

        [Test]
        public void DropdownList()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/dropdown");
            SelectElement select = new SelectElement(driver.FindElement(By.Id("dropdown")));
            select.SelectByIndex(1);
            Assert.That(select.SelectedOption.Text.Equals("Option 1"));
        }

        [Test]
        public void ContextMenu()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/context_menu");
            Actions actions = new Actions(driver);
            actions.ContextClick(driver.FindElement(By.XPath("//div[@id='hot-spot']"))).Perform();
            Assert.That(driver.SwitchTo().Alert().Text.Equals("You selected a context menu"));
        }

        [Test]
        public void Checkboxes()
        {
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/checkboxes");
            driver.FindElement(By.TagName("input")).Click();
            Assert.That(driver.FindElement(By.TagName("input")).GetAttribute("checked").Equals(true));
        }

        [Test]
        public void BasicAuth()
        {
            driver.Navigate().GoToUrl("https://admin:admin@the-internet.herokuapp.com/basic_auth");
            Assert.That(driver.FindElement(By.TagName("h3")).Text.Equals("Basic Auth"));
        }

        [Test]
        public void AddRemoveElements()
        {
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/add_remove_elements/");
            driver.FindElement(By.TagName("button")).Click();
            Assert.That(driver.FindElement(By.CssSelector(".added-manually")).Enabled.Equals(true));
            driver.FindElement(By.CssSelector(".added-manually")).Click();
        }

        [Test]
        public void MultipleWindowsTest()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/windows");
            driver.FindElement(By.CssSelector(".example a")).Click();
            var allWindows = driver.WindowHandles;
            driver.SwitchTo().Window(allWindows[0].ToString());
            Assert.That(!driver.Title.Equals("New Window"));
            driver.SwitchTo().Window(allWindows[1].ToString());
            Assert.That(driver.Title.Equals("New Window"));
        }

        
    }
}